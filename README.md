# Paul's Libs
Une simple librairie C++ regroupant des fonctions mathématiques, de graphisme, de réseautage et d'intelligence artificelle que je suis amener à utiliser dans mes projets. Cette librairie est une librairie de fichier headers, donc il n'y a pas de compilation particulière à faire. Sympa non ?

**pour des raisons de performance, Pauls Lib propose version de la librairie mathématique baséé sur Eigen.**. Cette fonctionnalitée peut être activée dans le fichier `<PLibs/Math>` de la librairie. (elle n'est pas activée par défault).

## Fonctionnalitées
Voici toutes les différentes branches dont la librairie se compose. J'essaie d'optimiser au maximum mes algorithmes, mais parfois l'optimisation maximale est hors de porté pour moi. 

### MATHS
#### Algèbre linéaire
##### Matrices
 - [x] zeros (amtrice de zeros)
 - [x] ones (matrice de 1)
 - [x] id (matrice identité)
 - [ ] rand (matrice random)
 - [ ] randn (matrice random avec distribution normale)
 
##### Produits de matrices et vecteurs
 - [ ] dot
 - [ ] outer
 - [x] matmul (operator *)
 - [ ] einsum
 - [ ] mawtpow (operator ^)
 - [ ] kron

##### Decomposition
 - [ ] cholesky (Decomposition de Cholesky)
 - [ ] qrfact (QR factorisation)
 - [ ] lufact (LU factorisation)
 - [ ] svd

##### Vecteurs propres
 - [ ] vp (vecteurs propres et valeurs propres)
 - [ ] vpm (matrice réduite)
 - [ ] gvp (vecteurs propres et valeurs propre d'une matrice general)

##### Normes
 - [ ] norm (norme de la matrice)
 - [ ] matcond (condition de la matrice)
 - [ ] det (determinant de la matrice) 
 - [ ] matrank (le rang de la matrice)
 - [x] trace (trace de la matrice)

##### Résolution et inversion
 - [ ] solve (resoud un systeme linéaire)
 - [ ] lstsq (retourne méthode des moindre carrés)
 - [x] matinv (inverse la matrice)
 - [ ] pinv (pseudo inverse de la matrice)

#### Chiffrement
##### Arithmétique du chiffrement
 - [ ] operations elementaires sur des nombres infinis
 - [ ] modulo

##### Aléatoire
 - [ ] RNG
 - [ ] Normal distribution
 - [ ] Noise
 - [ ] Fractal Noise

#### Cryptage
##### Algorithmes
 - [ ] MD5
 - [ ] SHA 1
 - [ ] SHA 256

### Image
#### Fenetre
#### Vidéos
##### MP4
##### MOV
##### PMOV
#### Images
##### JPEG
##### PNG
##### SVG
##### PIMG
#### Manipulation d'images

### Web
#### Serveur HTTP
#### RestAPI
##### Routage
##### Request

### Réseau de neurones
#### FFNN
