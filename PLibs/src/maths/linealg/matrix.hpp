#ifndef PL_LINEALG_MATRIX_H
#define PL_LINEALG_MATRIX_H

namespace plibs {
  // type de la matrice, colonnes, lignes
  template<typename T, int cols, int rows>
  struct Matrix {
    T data[cols][rows];

    // initialisation de la matrice
    Matrix () {
      for (int i = 0; i < rows; i++) 
	for (int j = 0; j < cols; j++) 
	  data[i][j] = 0;
    }

    // initialisation de la matrice par des vecteurs (struct de matrice)
    Matrix (std::vector<std::vector<T>> base) {
      // on verifie que la taille est bien celle attendue
      if (base.size() != cols) {
	throw std::invalid_argument("PLIBS ERROR : You can't initalize Matrice with vector of different dimension");
      }

      if (base.at(0).size() != rows) {
	throw std::invalid_argument("PLIBS ERROR : You can't initalize Matrice with vector of different dimension");
      }
      
      for (int i = 0; i < cols; i++)
	for (int j = 0; j < rows; j++)
	  data[i][j] = base.at(i).at(j);
    }

    Matrix (T liste[cols][rows]) {
      for(int i = 0; i < cols; i++)
	for(int j = 0; j < rows; j++)
	  data[i][j] = liste[i][j];
    }

    friend std::ostream& operator << (std::ostream& stream, const Matrix& mat) {
      int maxSize = 0;
      for (int i = 0; i < cols; i++) {
	for (int j = 0; j < rows; j++) {
	  std::string n = std::to_string(abs(mat.data[i][j]));
	  int l = n.length();
	  if (mat.data[i][j] < 0)
	    l++;
			 
	  (l > maxSize) ? maxSize = l : maxSize = maxSize;
	}
      }
			 
	       
      stream << "[";
      for (int i = 0; i < cols; i++) {
	(i == 0) ? stream << "[" : stream << " [";
	for (int j = 0; j < rows; j++) {
	  for (int k = std::to_string(mat.data[i][j]).length(); k < maxSize; k++)
	    stream << " ";
	  stream << std::to_string(mat.data[i][j]);
	  (j == rows - 1) ? stream << "" : stream << " ";
	}
	(i == cols - 1) ? stream << "]" : stream << "]\n";
      }
      stream << "]";
    }

    friend Matrix operator + (Matrix mat1, Matrix mat2) {
      Matrix mat;
      for (int i = 0; i < cols; i++)
	for (int j = 0; j < rows; j++)
	  mat.data[i][j] = mat1.data[i][j] + mat2.data[i][j];
      return mat;
    }

    friend Matrix operator += (Matrix mat1, Matrix mat2) {
      return mat1 + mat2;
    }

    friend Matrix operator - (Matrix mat1, Matrix mat2) {
      Matrix mat;
      for (int i = 0; i < cols; i++)
	for (int j = 0; j < rows; j++)
	  mat.data[i][j] = mat1.data[i][j] - mat2.data[i][j];
      return mat;
    }

    friend Matrix operator -= (Matrix mat1, Matrix mat2) {
      return mat1 - mat2;
    }

    friend Matrix operator * (Matrix mat1, T lambda) {
      Matrix mat;
      for (int i = 0; i < cols; i++)
	for (int j = 0; j < rows; j++)
	  mat.data[i][j] = mat1.data[i][j] * lambda;
      return mat;
    }

    friend Matrix operator *= (Matrix mat1, T lambda) {
      return mat1 * lambda;
    }

    friend Matrix operator / (Matrix mat1, T lambda) {
      if (lambda == 0) {
	throw std::invalid_argument("PLIBS ERROR : Division by 0");
      }
      Matrix<T, cols, rows> mat;
      for (int i = 0; i < cols; i++)
	for (int j = 0; j < rows; j++)
	  mat.data[i][j] = mat1.data[i][j] * lambda;
      return mat;
    }

    friend Matrix operator /= (Matrix mat1, T lambda) {
      return mat1 / lambda;
    }
        
    template<int cols2, int rows2>
    inline Matrix<T, cols, rows2> operator * (Matrix<T, cols2, rows2> mat2) {
      T sum = 0;
      Matrix<T, cols, rows2> finalMat;

      for(int i = 0; i < cols; i++){
	for(int j = 0; j < rows2; j++) {
	  sum = 0;
	  for (int k = 0; k < cols2; k++) {
	    sum += data[i][k] * mat2.data[k][j];
	  }
	  finalMat.data[i][j] = sum;
	}
      }

      return finalMat;
    }

    inline Matrix<T, cols, rows> identity () {
      Matrix<T, rows, cols> id;
      if (cols != rows) {	
	throw std::invalid_argument("PLIBS ERROR : Identity must be square");
      }
      
      for (int i = 0; i < cols; i++){
	for (int j = 0; j < rows; j++){
	  if (i == j){
	    id.data[i][j] = 1;
	  } else {
	    id.data[i][j] = 0;	    
	  }
	}
      }

      return id;
    }
	  
    inline Matrix<double, cols, rows> inverse () {
      if (cols != rows) {
	throw std::invalid_argument("PLIBS ERROR : You can't inverse a non-squared matrix");
      }
      const int n = 2*cols;
      const int m = rows;
      Matrix<double, m, n> inv;

      for (int i = 0; i < n; i++) {
	for (int j = 0; j < m; j++) {
	  if (i < n / 2) {
	    inv.data[j][i] = data[j][i];
	  } else {
	    if ((i - (n / 2)) == j) {
	      inv.data[j][i] = 1;
	    }
	  }
	}
      }

      // creation de la matrice echelonné
      for (int j = 0; j < m; j++) { // on fait toutes les colonnes
	// on cherche le pivot
	if (inv.data[j][j] == 0) {
	  // on trouve un pivot et on echange sa ligne avec la ligne j, si on trouve pas de pivot,
	  // la matrice n'est pas inversible car pas carrée.
	  int pivotPosition = 0;
			 
	  for (int l = 0; l < m; l++) {
	    if (inv.data[l][j] != 0) {
	      pivotPosition = l;
	      break;
	    }
	  }

	  // on a trouve le pivot, on echange les lignes pivotPosition et j
	  std::swap(inv.data[j], inv.data[pivotPosition]);
	}
		    
	double pivot = inv.data[j][j];
	// on normalise la ligne sur laquel on travail
	for (int i = 0; i < n; i++) {
	  inv.data[j][i] = inv.data[j][i] * (1 / pivot);
	}

	// on créé des 0 sur les autres lignes de la colonne
	for (int i = j + 1; i < m; i++) { // pour chaque lignes
	  double tmpPivot = inv.data[i][j];
	  for (int l = 0; l < n; l++) { // pour toutes les colonnes de la ligne
	    inv.data[i][l] = inv.data[i][l] - tmpPivot * inv.data[j][l];
	  }
	}
      }

      for (int i = m - 1; i >= 0; i--) { // on part du bas pour enlever les termes non 0, on travaille sur la colonne i    
	for (int j = 0; j < i; j++) { // on parcourt toute la ligne en partant de la droite -> pivot
	  double tmpPivot = inv.data[j][i];
	  for (int k = 0; k < n; k++) {
	    inv.data[j][k] -= tmpPivot * inv.data[i][k];
	  }
	}
      }
	       
      Matrix<double, cols, rows> fin;

      for (int i = n/2; i < n; i++) {
	for (int j = 0; j < rows; j++) {
	  fin.data[j][i - n/2] = inv.data[j][i];
	}
      }

      return fin;
    }

    inline Matrix<T, rows, cols> transpose () {
      Matrix<T, rows, cols> fin;
      for (int i = 0; i < cols; i++) {
	for (int j = 0; j < rows; j++) {
	  fin.data[j][i] = data[i][j];
	}
      }

      return fin;
    }

    inline T sum () {
      T sum;
      for (int i = 0; i < cols; i++) {
	for (int j = 0; j < rows; j++) {
	  sum += data[i][j];
	}
      }

      return sum;
    }

    inline T prod () {
      T prod;
      for (int i = 0; i < cols; i++) {
	for (int j = 0; j < rows; j++) {
	  prod *= data[i][j];
	}
      }

      return prod;
    }

    inline T mean () {
      T sum = this->sum();
      return sum / (cols * rows);
    }

    inline T minCoeff () {
      T min = data[0][0];
      for (int i = 0; i < cols; i++) {
	for (int j = 0; j < rows; j++) {
	  if (min > data[i][j]) {
	    min = data[i][j];
	  }
	}
      }

      return min;	       
    }

    inline T maxCoeff () {
      T max = data[0][0];
      for (int i = 0; i < cols; i++) {
	for (int j = 0; j < rows; j++) {
	  if (max < data[i][j]) {
	    max = data[i][j];
	  }
	}
      }

      return max;	       	       
    }

    inline T trace () {
      T trace;
      for (int i = 0; i < cols; i++) {
	trace += data[i][i];
      }

      return trace;
    }
  };

  typedef Matrix<int, 2, 2> Matrix22i;
  typedef Matrix<float, 2, 2> Matrix22f;
  typedef Matrix<double, 2, 2> Matrix22d;
     
  typedef Matrix<int, 3, 3> Matrix33i;
  typedef Matrix<float, 3, 3> Matrix33f;
  typedef Matrix<double, 3, 3> Matrix33d;

  typedef Matrix<int, 4, 4> Matrix44i;
  typedef Matrix<float, 4, 4> Matrix44f;
  typedef Matrix<double, 4, 4> Matrix44d;
}

#endif
