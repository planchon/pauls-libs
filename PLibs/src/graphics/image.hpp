namespace plibs {
     template<int Twidth, int Theight>
     struct Image {
	  bool SDL_Driven = false;
	  SDL_Surface* SDL_Data = NULL;
	  
	  int width  = Twidth;
	  int height = Theight;
	  
	  int data[Twidth * Theight];
	  Image () {
	       for (int i = 0; i < Twidth; i++) {
		    for (int j = 0; j < Theight; j++) {
			 data[i * Theight + j] = 0;
		    }
	       }
	  }
	  
	  inline void importImage (char* pathToImage) {
	       SDL_Driven = true;
	       SDL_Data = IMG_Load(pathToImage);
	       if (!SDL_Data) {
		    std::cout << "ERROR : " << SDL_GetError() << std::endl;
		    throw std::invalid_argument("PLIBS : Image openning error.");
	       }

	       width = SDL_Data->w;
	       height = SDL_Data->h;
	  }

	  inline void destroy() {
	       if (SDL_Driven == true) {
		    SDL_FreeSurface(SDL_Data);
	       }
	  }
     };

     struct Text {
	  char* text = "";
	  
	  Text (char* text) {
	       text = text;
	       //check if latex is install on the system
	       if(system("which latex > /dev/null 2>&1")) {
		    throw std::invalid_argument("PLIBS : Cant render text, LaTeX not installed");
	       } else {
		    std::cout << "plibs -> Latex found" << std::endl;
	       }

	       if(system("which dvisvgm > /dev/null 2>&1")) {
		    throw std::invalid_argument("PLIBS : Cant render text, dvisvgm not installed");
	       } else {
		    std::cout << "plibs -> dvisvgm found" << std::endl;
	       }
	  }

	  inline void display() {
	       //1 do the svg render to tmp folder
	       //2 interpret the svg file
	       //3 render to image buffer
	       //4 render image
	  }
     };
     
     template<int	Twidth, int Theight>
     struct Window {
	  SDL_Window*	window	      = NULL;
	  SDL_Surface*	screenSurface = NULL;
	  SDL_Renderer* renderer      = NULL;
	  TTF_Font*	Sans24	      = NULL;
	  char*		title	      = "PLIBS Window interface";
	  int		width	      = Twidth;
	  int		height	      = Theight;
	  
	  Window () {

	       //Initialize SDL
	       if( SDL_Init(SDL_INIT_VIDEO) < 0 ) {
		    printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
	       } else {
		    if ((TTF_Init()) < 0) {
			 std::cout << TTF_GetError() << std::endl;
		    }
		    if ((this->Sans24 = TTF_OpenFont("FreeSans.ttf", 24)) < 0) {
			 std::cout << TTF_GetError() << std::endl;
		    }
		    
		    //Create window
		    this->window   = SDL_CreateWindow(this->title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, this->width, this->height, SDL_WINDOW_SHOWN);

		    this->renderer = SDL_CreateRenderer(this->window, -1, SDL_RENDERER_ACCELERATED);

		    if( this->window == NULL ) {
			 printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
		    } else {
			 //Get window surface
			 this->screenSurface = SDL_GetWindowSurface(this->window);
			 
			 //Update the surface
			 SDL_UpdateWindowSurface(this->window);
		    }
	       }

	       std::cout << "plibs -> sdl window created" << std::endl;
	  }

	  inline void text(char* text, int x, int y) {
	       SDL_Color White = {255, 255, 255};
	       if (this->Sans24 == NULL) {
		    std::cout << TTF_GetError() << std::endl;
	       }
	       // SDL_Surface* textSurface = TTF_RenderText_Solid(this->Sans24, text, White);
	       // SDL_Texture* textTexture = SDL_CreateTextureFromSurface(this->renderer, textSurface);
	       // SDL_Rect pos = {x,y,100,100};

	       // SDL_RenderCopy(this->renderer, textTexture, NULL, &pos);
	  }

	  template<int w, int h>
	  inline void display(Image<w, h> image) {
	       if (!image.SDL_Driven) {
		    if ((w != this->width) or (h != this->height)) {
			 throw std::invalid_argument("PLIBS ERROR : Cannot display screen of different size using display(image)");
		    }

		    SDL_Texture* texture = SDL_CreateTexture(this->renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, w, h);
	       
		    SDL_RenderClear(this->renderer);
		    SDL_UpdateTexture(texture, NULL, &image.data[0], w * 4);
		    SDL_RenderCopy(this->renderer, texture, NULL, NULL);
		    SDL_RenderPresent(this->renderer);

		    SDL_DestroyTexture(texture);
	       } else {
		    SDL_Texture* texture = SDL_CreateTextureFromSurface(this->renderer, image.SDL_Data);
		    SDL_Rect pos = {0,0,image.width, image.height};
		    SDL_RenderCopy(this->renderer, texture, NULL, &pos);
		    SDL_RenderPresent(this->renderer);
	       }
	  }

	  template<int w, int h>
	  inline void display(Image<w, h> image, int x, int y) {
	       if (!image.SDL_Driven) {
		    if ((w > this->width) or (h > this->height)) {
			 throw std::invalid_argument("PLIBS ERROR : Cannot display screen of bigger size");
		    }

		    Image<Twidth, Theight> buffer;
	       
		    for(int i = 0; i < this->width; i++) {
			 for (int j = 0; j < this->height; j++) {
			      if ((i >= x) and (j >= y) and (j < y + h) and (i < w + x)) {
				   buffer.data[i * this->height + j] = image.data[i * w + j];
			      } else {
				   buffer.data[i * this->height + j] = 0;
			      }
			 }
		    }

		    display(buffer);
	       } else {
		    SDL_Texture* texture = SDL_CreateTextureFromSurface(this->renderer, image.SDL_Data);
		    SDL_Rect pos = {x,y,image.width, image.height};
		    SDL_RenderCopy(this->renderer, texture, NULL, &pos);
		    SDL_RenderPresent(this->renderer);
	       }
	  }

	  inline void updateScreen() {
	       SDL_UpdateWindowSurface(this->window);
	  }
	  
	  inline void destroy () {
	       SDL_DestroyWindow(window);
	       SDL_Quit();
	       std::cout << "plibs -> sdl window destroy, that sad :c" << std::endl;
	  }

	  inline bool shouldQuit () {
	       SDL_Event event;

	       if (SDL_PollEvent(&event)) {
		    switch (event.type) {
		    case SDL_QUIT:    
			 return false;
		    }
	       }

	       return true;
	  }
     };
}
