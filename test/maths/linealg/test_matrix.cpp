#include <PLibs/Maths>
#include <iostream>

bool test_matric_data_initialisation () {
  std::cout << "PLIBS_TEST :: MATRIX :: Matrix data initialization" << std::endl;
  std::cout << "PLIBS_TEST :: MATRIX :: test 1 -> ";

  plibs::Matrix<int, 3, 3> mat_int;

  bool status = true;

  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      if (mat_int.data[i][j] != 0) {	
	std::cout << "failed";
	status = false;
      }
    }
  }
  
  if (status) std::cout << "passed" << std::endl;

  if (status) std::cout << "PLIBS_TEST :: MATRIX :: All Matrix data initialization test passed\n\n";
  if (!status) std::cout << "PLIBS_TEST :: MATRIX :: A Matrix data initialization test failed\n\n";
  
  return status;
}

bool test_matrix_vector_initialisation() {
  // les tests sont :
  //   - matrice normal (1)
  //   - matrice avec un probleme de dimension (2 et 3)
  
  bool total = false;

  std::cout << "PLIBS_TEST :: MATRIX :: Matrix vector initialization" << std::endl;
  std::cout << "PLIBS_TEST :: MATRIX :: test 1 -> ";

  // basique
  std::vector<std::vector<int>> int_vec33 = {{1,2,3},{4,5,6},{7,8,9}};
  plibs::Matrix<int, 3, 3> mat_int_vec33 (int_vec33);
  bool status = true;
  
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      if (int_vec33.at(i).at(j) != mat_int_vec33.data[i][j]) {	
	std::cout << "failed";
	status = false;
	total = total || true;
      }
    }
  }
  if (status) std::cout << "passed" << std::endl;

  // une matrice impossible
  std::vector<std::vector<int>> int_vec23 = {{1,2,3},{4,5,6}};
  status = true;
  std::cout << "PLIBS_TEST :: MATRIX :: test 2 -> ";
  
  try {
    plibs::Matrix<int, 3, 3> mat_int_vec23 (int_vec23);
  } catch (const std::invalid_argument &e) {
    if (e.what() != "PLIBS ERROR : You can't initalize Matrice with vector of different dimension") {
      status = false;
      total = total || true;
      std::cout << "passed" << std::endl;
    }
  }
  if (status) std::cout << "failed" << std::endl;

  // une matrice impossible
  std::vector<std::vector<int>> int_vec22 = {{1,2,3},{4,5,6}};
  status = true;
  std::cout << "PLIBS_TEST :: MATRIX :: test 3 -> ";
  
  try {
    plibs::Matrix<int, 2, 2> mat_int_vec22 (int_vec22);
  } catch (const std::invalid_argument &e) {
    if (e.what() != "PLIBS ERROR : You can't initalize Matrice with vector of different dimension") {
      status = false;
      total = total || true;
      std::cout << "passed" << std::endl;
    }
  }
  if (status) std::cout << "failed" << std::endl;

  if (total) std::cout << "PLIBS_TEST :: MATRIX :: All Matrix vector initialization test passed\n\n";
  if (!total) std::cout << "PLIBS_TEST :: MATRIX :: A Matrix vector initialization test failed\n\nx";
  return !total;
}

bool test_matrix_list_initialization() {
  std::cout << "PLIBS_TEST :: MATRIX :: Matrix list initialization" << std::endl;
  std::cout << "PLIBS_TEST :: MATRIX :: test 1 -> ";
}

int main() {
  test_matric_data_initialisation();
  test_matrix_vector_initialisation();
  //test_matrix_list_initialization();
}
