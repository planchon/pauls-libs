#include <PLibs/Maths>

int main() {
     plibs::Matrix33d matrice ({{1,2,3},{1,5,6},{7,8,9}});
     plibs::Matrix22d matrice2 ({{1,2},{3,4}});
     std::cout << matrice2.mean() << std::endl;
     return 0;
}
