#include <PLibs/Graphics>

int main() {
     plibs::Window<500,500> window;
     plibs::Image<300, 300> image;

     window.text("salut", 10, 10);
     window.updateScreen();
     
     while (window.shouldQuit()) {}

     window.destroy();
     
     return 0;
}
