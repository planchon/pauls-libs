CC=g++
FLAGS=-std=c++11
GRAPHICS = -lSDL2 -lSDL2_image -lSDL2_ttf
INCLUDE= . 

testVector:
	$(CC) test/maths/testVector.cpp -I $(INCLUDE) $(FLAGS) -o bin/test/testVector

testMatrix:
	$(CC) test/maths/testMatrix.cpp -I $(INCLUDE) $(FLAGS)  -o bin/test/testMatrix

testGraphics:
	$(CC) test/graphics/main.cpp -I $(INCLUDE) $(FLAGS) $(GRAPHICS)  -o bin/test/testGraphics
